package com.coba.rosy.jsonlistview.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.coba.rosy.jsonlistview.BaseAPI;
import com.coba.rosy.jsonlistview.BaseAPI_;
import com.coba.rosy.jsonlistview.R;
import com.coba.rosy.jsonlistview.model.ModelPhoto;
import com.coba.rosy.jsonlistview.view.adapter.MainAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    BaseAPI baseAPI;
    ListView listView;
   ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main  );
        baseAPI = BaseAPI.Factory.create();

        assignUEIElements();
        loadphoto();
    }

    private void assignUEIElements(){
        listView = (ListView) findViewById(R.id.ListView);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.GONE);

    }

    private void loadphoto(){
        showProgress();
        Call<List<ModelPhoto>> photolist = baseAPI.getPhotoList();
        photolist.enqueue(new Callback<List<ModelPhoto>>() {
            @Override
            public void onResponse(Call<List<ModelPhoto>> call, Response<List<ModelPhoto>> response) {
                MainAdapter adapter = new MainAdapter(MainActivity.this, response.body());
                listView.setAdapter(adapter);
                closeProgeress();
            }

            @Override
            public void onFailure(Call<List<ModelPhoto>> call, Throwable t) {
                System.out.println("Gagal Load Foto");
                System.out.println(">> "+t.getMessage());
            }
        });

    }

    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);

    }
    private void closeProgeress(){
        progressBar.setVisibility(View.GONE);
    }


}



